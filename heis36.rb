#!/bin/env ruby
# heis36 in ruby # Junji Nishino 2016/8/18 nishinojunji@uec.ac.jp
# Heis : very simple units TSG.
# Teams A vs B with 36 unis
# Board 20x20
# Opening position A[(14,0)-(19,5)], B[(0,14)-(5,19)]
# Unit has 2HP, movable 3steps in turn, 1hit against neighbor
# all Unit can move and hit in one turn

$bsize = 20
$empbox = ". "
$team1  = "A2"
$team2  = "B2"



class Heisboard
  def initialize
    @board=Array.new
    clearboard
    fillrect(14,0, 19,5, $team1)
    fillrect(0,14, 5,19, $team2)
  end

  def clearboard
    ($bsize*$bsize).times{|x|
      @board[x] = $empbox
    }
  end

  def xy2addr(x,y)
    return x + y * $bsize
  end

  def addr2xy(addr)
    return [addr % $bsize, addr / $bsize]
  end

  def mdist(s, d)
    xs, ys = addr2xy(s)
    xd, yd = addr2xy(d)
    return (xs - xd + ys - yd).abs
  end


  def setpiece(x,y, p)
    @board[xy2addr(x,y)] = p
  end

  def show
    print "   "
    $bsize.times{|x|
      printf "%2d", x
    }
    ($bsize*$bsize).times{|x|
      if ( (x) % $bsize == 0)
        print "\n"
        printf "%2d ", (x)/$bsize
      end
      print @board[x]
    }
    print "\n"
  end

  def isempty(x)
    @board[x] == $empbox
  end

  def move(s, d) # s, d : addr
    if !isempty(d)
      p "no"
      return
    end
    if !(mdist(s, d) <= 3)
      p "too far"
      return
    end

    @board[d]=@board[s]
    @board[s] = $empbox
  end

  def movexy(sx, sy, dx, dy)
    move(xy2addr(sx,sy), xy2addr(dx,dy))
  end

  def fillrect(x1,y1,x2,y2, p)
    xl = [x1,x2].min
    yl = [y1,y2].min
    xh = [x1,x2].max
    yh = [y1,y2].max
    (yl..yh).each{|y|
      (xl..xh).each{|x|
        @board[xy2addr(x,y)] = p
      }
    }
  end

end


# ----- func for main
#

def nnrand(m)
  return true if (rand (3*m)) %m == 0
  return false
end

def turnend
  return nnrand(2)
end

def gameend
  return nnrand(4)
end

def moveread
end


# ----- main

def main
  h = Heisboard.new
  h.show

  h.move(19, 0)
  h.show

  h.move(h.xy2addr(14,5), h.xy2addr(13,5))
  h.show

  h.movexy(15,5, 14,5)
  h.show

=begin
  while(true)
    while(true)
      p "turn A"
      break if turnend()
      h.show
      m=moveread
      h.move(m[0],m[1],m[2])
      h.show
    end

    while(true)
      p "turn B"
      break if turnend()
    end


  break if gameend()

  end
=end

end

main

